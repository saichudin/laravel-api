<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $faker = Faker\Factory::create(); //import library Faker
        $limit = 20; //batasan banyak data

        for($i=0;$i<$limit;$i++)
        {
          DB::table('kontak')->insert([
            'nama'  => $faker->name,
            'email' => $faker->unique()->email,
            'nohp'  => $faker->phoneNumber,
            'alamat'=> $faker->address,
          ]);
        }
    }
}

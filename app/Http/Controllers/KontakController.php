<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kontak;

class KontakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data = Kontak::all();

        if (count($data) > 0) {
          $res['message'] = 'Success!';
          $res['values'] = $data;
          return response($res);
        }
        else {
          $res['message'] = 'Empty!';
          return response($res);
        }
    }

    public function show($id)
    {
      $data = Kontak::where('id',$id)->get();

      if (count($data) > 0) {
        $res['message'] = 'Success!';
        $res['values'] = $data;
        return response($res);
      }
      else {
        $res['message'] = 'Empty!';
        return response($res);
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = New Kontak();
        $data->nama = $request->nama;
        $data->email = $request->email;
        $data->nohp = $request->nohp;
        $data->alamat = $request->alamat;

        if ($data->save()){
          $res['message'] = "Saved!";
          $res['values'] = $data;
          return response($res);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = Kontak::where('id',$id)->first();
        $data->nama = $request->nama;
        $data->email = $request->email;
        $data->nohp = $request->nohp;
        $data->alamat = $request->alamat;

        if ($data->save()){
          $res['message'] = 'Success!';
          $res['values'] = $data;
          return response($res);
        }
        else {
          $res ['message'] = 'Failed!';
          return response($res);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $data = Kontak::where('id',$id)->first();

        if($data->delete()){
          $res['message'] = 'Deleted!';
          $res['values'] = $data;
          return response($res);
        }
        else {
          $res ['message'] = 'Failed!';
          return response($res);
        }
    }
}
